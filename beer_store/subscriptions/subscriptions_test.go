package subscriptions

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alibitek-go/go-testing-playground/beerstore"
)

func TestStartSubcription(t *testing.T) {
	cart1 := &beerstore.Cart{
		Cases: []*beerstore.Case{
			beerstore.FixtureCase(4, beerstore.FixtureBeer("Duvel", "Tripel Hop", 11.0), 14),
		},
	}
	cart2 := &beerstore.Cart{
		Cases: []*beerstore.Case{
			beerstore.FixtureCase(4, beerstore.FixtureBeer("Labatt", "Blue Light", 12.0), 24),
		},
	}
	subscription := &Subscription{
		Cart:        cart1,
		Interval:    time.Duration(1) * time.Second,
		messageChan: make(chan interface{}),
	}

	go subscription.startSubscriptionTimer(context.Background())

	// cart1
	msg := <-subscription.messageChan
	order, ok := msg.(*beerstore.Cart)
	if !ok {
		t.Fatal("received invalid message on message channel")
	}
	assert.Equal(t, cart1, order)

	// cart2
	subscription.SetCart(cart2)
	msg = <-subscription.messageChan
	order, ok = msg.(*beerstore.Cart)
	if !ok {
		t.Fatal("received invalid message on message channel")
	}
	assert.Equal(t, cart2, order)
}
