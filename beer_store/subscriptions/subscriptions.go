package subscriptions

import (
	"context"
	"sync"
	"time"

	"gitlab.com/alibitek-go/go-testing-playground/beerstore"
)

// Subscription represents a subscription for a shopping cart.
type Subscription struct {
	Cart        *beerstore.Cart
	Interval    time.Duration
	messageChan chan interface{}
	mutex       sync.Mutex
}

// startSubscriptionTimer starts a timer and fires the cart to the order handler when order is ready
func (s *Subscription) startSubscriptionTimer(ctx context.Context) {
	ticker := time.NewTicker(s.Interval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			s.messageChan <- s.GetCart()
		}
	}
}

// GetCart safely retrives the subscriptions shopping cart
func (s *Subscription) GetCart() *beerstore.Cart {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.Cart
}

// SetCart safeky sets the subcriptions shopping cart
func (s *Subscription) SetCart(cart *beerstore.Cart) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.Cart = cart
}
