package beerstore

// Cart represents a shopping cart.
type Cart struct {
	Cases []*Case
}

// NewCart creates a new empty cart.
func NewCart() *Cart {
	return &Cart{}
}

// Case represents a case/pack of beer.
type Case struct {
	Count int
	Beer  *Beer
	Price float64
}

// Beer represents a type of beer
type Beer struct {
	Brand  string
	Name   string
	Ounces float64
}

// AddCase adds a case of beer to the shopping cart
func (c *Cart) AddCase(beerCase *Case) {
	c.Cases = append(c.Cases, beerCase)
}

// Subtotal calculates the subtotal of the shopping cart.
func (c *Cart) Subtotal() float64 {
	var subTotal float64
	for _, beerCase := range c.Cases {
		subTotal += beerCase.Price
	}
	return subTotal
}

// FixtureBeer creates a Beer fixture for use in tests.
func FixtureBeer(brand string, name string, ounces float64) *Beer {
	return &Beer{
		Brand:  brand,
		Name:   name,
		Ounces: ounces,
	}
}

// FixtureCase creates a Case fixture for use in tests.
func FixtureCase(count int, beer *Beer, price float64) *Case {
	return &Case{
		Count: count,
		Beer:  beer,
		Price: price,
	}
}

// FixtureCart creates a Cart fixture for use in tests.
func FixtureCart() *Cart {
	return &Cart{
		Cases: []*Case{},
	}
}
