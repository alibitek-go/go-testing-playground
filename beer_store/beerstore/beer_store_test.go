package beerstore

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddCaseTesting(t *testing.T) {
	cart := NewCart()
	if len(cart.Cases) != 0 {
		t.Fatal("expected empty cart")
	}

	blueLight := FixtureBeer("Labatt", "Blue Light", 12.0)
	cart.AddCase(FixtureCase(6, blueLight, 10.99))

	if len(cart.Cases) != 1 {
		t.Fatal("expected 1 case in cart")
	}
}

func TestAddCaseAssert(t *testing.T) {
	cart := NewCart()
	assert.Equal(t, 0, len(cart.Cases), "expected empty cart")

	blueLight := FixtureBeer("Labatt", "Blue Light", 12.0)
	cart.AddCase(FixtureCase(6, blueLight, 10.99))

	assert.Equal(t, 1, len(cart.Cases), "expected 1 case in cart")
}

func TestSubtotal(t *testing.T) {
	cart := NewCart()
	assert.Equal(t, 0, len(cart.Cases), "expected empty cart")

	duvelHop := FixtureBeer("Duvel", "Tripel Hop", 11.0)
	cart.AddCase(FixtureCase(4, duvelHop, 14.99))

	blueLight := FixtureBeer("Labatt", "Blue Light", 12.0)
	cart.AddCase(FixtureCase(30, blueLight, 24.99))

	assert.Equal(t, 39.98, cart.Subtotal())
}

func TestSubtotalSuite(t *testing.T) {
	testCases := []struct {
		name     string
		cart     *Cart
		subTotal float64
	}{
		{
			name:     "Empty cart",
			cart:     &Cart{},
			subTotal: 0,
		},
		{
			name: "Party time",
			cart: &Cart{
				Cases: []*Case{
					FixtureCase(4, FixtureBeer("Duvel", "Tripel Hop", 11.0), 14.99),
					FixtureCase(30, FixtureBeer("Labatt", "Blue Light", 12.0), 24.99),
					FixtureCase(30, FixtureBeer("Labatt", "Blue Light", 12.0), 24.99),
				},
			},
			subTotal: 64.97,
		},
		{
			name: "Negative",
			cart: &Cart{
				Cases: []*Case{
					FixtureCase(4, FixtureBeer("Duvel", "Tripel Hop", 11.0), -14),
					FixtureCase(30, FixtureBeer("Labatt", "Blue Light", 12.0), 24),
				},
			},
			subTotal: 10.00,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			assert.Equal(t, testCase.subTotal, testCase.cart.Subtotal())
		})
	}
}
