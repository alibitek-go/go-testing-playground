package payments

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// ProcessPayment sends the total to an external payment API
func ProcessPayment(paymentServer string, total float64) ([]byte, error) {
	b, err := json.Marshal(total)
	if err != nil {
		return nil, err
	}

	response, err := http.Post(paymentServer, "application/json", bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode >= 400 {
		return nil, fmt.Errorf("payment server error: %d", response.StatusCode)
	}

	return ioutil.ReadAll(response.Body)
}
