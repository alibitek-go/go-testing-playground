package payments

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProcessPayment(t *testing.T) {
	testCases := []struct {
		name          string
		handler       http.HandlerFunc
		expectedError error
		expectedBody  []byte
	}{
		{
			name: "OK",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("OK"))
				w.WriteHeader(http.StatusOK)
			},
			expectedError: nil,
			expectedBody:  []byte("OK"),
		},
		{
			name: "Internal server error",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusInternalServerError)
			},
			expectedError: fmt.Errorf("payment server error: %d", http.StatusInternalServerError),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			testServer := httptest.NewServer(testCase.handler)
			defer testServer.Close()

			body, err := ProcessPayment(testServer.URL, 21.11)
			assert.Equal(t, testCase.expectedError, err)
			assert.Equal(t, testCase.expectedBody, body)
		})
	}
}
