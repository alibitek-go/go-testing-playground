package orders

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alibitek-go/go-testing-playground/beerstore"
)

func TestStartOrderHandler(t *testing.T) {
	handler := &OrderHandler{
		messageChan: make(chan interface{}),
	}
	go handler.startOrderHandler(context.Background())
	assert.Equal(t, 0, len(handler.ProcessedOrders))

	handler.messageChan <- beerstore.FixtureCart()
	handler.messageChan <- beerstore.FixtureCart()
	handler.messageChan <- beerstore.FixtureCase(30, beerstore.FixtureBeer("Labatt", "Blue Light", 12.0), 24)

	assert.Equal(t, 2, len(handler.ProcessedOrders))
}
