package orders

import (
	"context"
	"os"
	"sync"

	logger "github.com/sirupsen/logrus"
	"gitlab.com/alibitek-go/go-testing-playground/beerstore"
)

// OrderHandler represents the order handler data
type OrderHandler struct {
	messageChan     chan interface{}
	ProcessedOrders []*beerstore.Cart
	mutex           sync.RWMutex
}

// PlaceOrder places a new order tp be processed.
func (oh *OrderHandler) PlaceOrder(ctx context.Context, cart *beerstore.Cart) error {
	oh.mutex.Lock()
	oh.ProcessedOrders = append(oh.ProcessedOrders, cart)
	oh.mutex.Unlock()
	return nil
}

func init() {
	// Log as JSON instead of the default ASCII formatter.
	logger.SetFormatter(&logger.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logger.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logger.SetLevel(logger.DebugLevel)
}

// startOrderHandler waits for messages and processes them
func (oh *OrderHandler) startOrderHandler(ctx context.Context) {
	for {
		msg, ok := <-oh.messageChan
		if !ok {
			logger.Debug("message channel closed")
			return
		}

		cart, ok := msg.(*beerstore.Cart)
		if ok {
			if err := oh.PlaceOrder(ctx, cart); err != nil {
				logger.WithError(err).Error("error placing order")
				continue
			}

			logger.Info("successfully placed order")
			continue
		}

		logger.WithField("msg", msg).Errorf("received invalid message on message channel")
	}
}
